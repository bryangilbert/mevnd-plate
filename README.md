# Node Express Vue, Mongo with Docker -- MEVN-D

> a dockerized client server project 

See the project documentation on line in the GitHub Pages here:
[https://bryan-gilbert.github.io/mevnd-plate/](https://bryan-gilbert.github.io/mevnd-plate/)


To run and view the docs locally run
```
yarn docs:dev
```

Then visit  [http://localhost:8080/](http://localhost:8080/)
